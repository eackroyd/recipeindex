# Scrumptious Recipes

A web application allowing a user to post their favorite recipes, generate shopping lists and add ingredients from specified recipes, and navigate the site's global recipes for meal inspiration. 

## Stack

Backend: Django, SQLite
Frontend: HTML 5

# How to run Recipe Index

Follow these steps to set up and run the Recipe Index project on your local machine:

1. Clone the Repository:

   In your preferred terminal and desired directory, use the following command to clone the GitLab repository:

   ```
   git clone https://gitlab.com/eackroyd/recipeindex.git
   ```

2. Navigate to the Project Directory:

   Change your working directory to the project folder:

   ```
   cd recipeindex
   ```

3. Set up  and activate your virtual environment and run the web server:
    Execute the following commands to build and start the Docker containers and images:

    Windows:
    ```
    python -m venv .venv
    ./.venv/Scripts/Activate.ps1
    python manage.py runserver
    ```

    Mac OS:
    ```
    python -m venv .venv
    source ./.venv/bin/activate
    python manage.py runserver
    ```
    

4. Interact with the Application:

   To interact with the Recipe Index application, open your web browser and visit http://localhost:8000. Sign up to create a profile, and start adding recipes! Enjoy!