from django.shortcuts import render, get_object_or_404, redirect
import datetime
from datetime import timedelta

from recipes.models import Recipe, ShoppingListItem, ShoppingList
from recipes.forms import RecipeForm, IngredientForm, StepForm
from django.contrib.auth.decorators import login_required
# Create your views here.
#views take request from the browser interpret the request and retrieve any necessary data from your database and render the
#requested template

def redirect_to_recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes
    }
    return render(request, "recipes/list.html", context)

def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        if 'form1' in request.POST:
            form1 = IngredientForm(request.POST)
            if form1.is_valid():
                ingredient = form1.save(False)
                ingredient.recipe = recipe
                ingredient.save()
                return redirect('show_recipe', id=id)
        elif 'form2' in request.POST:
            form2 = StepForm(request.POST)
            if form2.is_valid():
                step = form2.save(False)
                step.recipe = recipe
                step.save()
                return redirect('show_recipe', id=id)
        elif 'form3' in request.POST:
            shopping_list = ShoppingList.objects.latest("id")
            for ingredient in recipe.ingredients.all():
                list_item = ShoppingListItem(amount=ingredient.amount, food_item=ingredient.food_item)
                list_item.shopping_list = shopping_list
                list_item.save()
            return redirect('shopping_list')
    else:
        form1= IngredientForm()
        form2=StepForm()

    context = {
        'recipe_object': recipe,
        'form1': form1,
        'form2':form2,
    }

    return render(request, "recipes/detail.html", context)



def breakfast_recipe_list(request):
    recipes = Recipe.objects.filter(type=1)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/breakfast.html", context)

def lunch_recipe_list(request):
    recipes = Recipe.objects.filter(type=2)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/lunch.html", context)

def dinner_recipe_list(request):
    recipes = Recipe.objects.filter(type=3)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/dinner.html", context)

def baked_goods_recipe_list(request):
    recipes = Recipe.objects.filter(type=4)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/baked_goods.html", context)

def recipe_list(request):
    return render(request, "recipes/list.html")

@login_required #apply the login_required decorator to the create_recipe view so that it is only accessible to authenitcated users
def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe=form.save(False) #call the form's save method passing false in order to create an instance of the recipe model with the submitted form data that we can modify before saving to the DB. this instance is NOT saved to the DB at this time.
            recipe.author = request.user # set the author field to the user who submitted the form
            recipe.save() # saving the updated instance of the recipe model to the DB
            return redirect("main_page")
    else:
        form = RecipeForm()
    context = {
        "form" : form,
    }
    return render(request, "recipes/create.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form=RecipeForm(request.POST, instance = recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form=RecipeForm(instance=recipe)

    context = {
        "recipe": recipe,
        "form": form
    }

    return render(request, "recipes/edit.html", context)

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes
    }

    return render(request, "recipes/my_list.html", context)


def view_shopping_list(request):
    shopping_list = ShoppingList.objects.latest("id")


    if request.method == "POST":
        shopping_list = ShoppingList()
        shopping_list.save()
        return redirect('main_page')


    context = {
        "shopping_list": shopping_list,
    }

    return render(request, "recipes/shopping_list.html", context)
