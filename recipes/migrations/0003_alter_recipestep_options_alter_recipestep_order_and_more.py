# Generated by Django 4.2.3 on 2023-07-18 02:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0002_recipestep'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipestep',
            options={'ordering': ['order']},
        ),
        migrations.AlterField(
            model_name='recipestep',
            name='order',
            field=models.PositiveSmallIntegerField(),
        ),
        migrations.CreateModel(
            name='Ingredients',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.TextField(max_length=100)),
                ('food_item', models.TextField(max_length=100)),
                ('recipe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ingredients', to='recipes.recipe')),
            ],
            options={
                'ordering': ['food_item'],
            },
        ),
    ]
