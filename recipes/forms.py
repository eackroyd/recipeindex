from django.forms import ModelForm
from recipes.models import Recipe, Ingredients, RecipeStep, ShoppingListItem

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "type",
        ]

class IngredientForm(ModelForm):

    class Meta:
        model = Ingredients
        fields = [
            "amount",
            "food_item",
        ]

class StepForm(ModelForm):

    class Meta:
        model = RecipeStep
        fields = [
            "instruction",
            "order",
        ]
