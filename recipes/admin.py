from django.contrib import admin
from recipes.models import Recipe, ShoppingList, TypeOfMeal
from recipes.models import RecipeStep, Ingredients, ShoppingListItem
# Register your models here.

#admin.site.register(Recipe)

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "title",
        "description",
        'picture',
        "created_on",
        "author",
        "type",
    ]

@admin.register(TypeOfMeal)
class TypeOfMealAdmin(admin.ModelAdmin):
    list_display = [
        "meal_type",
    ]

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = [
        "instruction",
        "order",
        "id",
    ]

@admin.register(Ingredients)
class IngredientsAdmin(admin.ModelAdmin):
    list_display = [
        "amount",
        "food_item",
        "id",
    ]

@admin.register(ShoppingList)
class ShoppingList(admin.ModelAdmin):
    list_display = [
        "date",
    ]

@admin.register(ShoppingListItem)
class ShoppingListItemAdmin(admin.ModelAdmin):
    list_display = [
        "amount",
        "food_item",
    ]
