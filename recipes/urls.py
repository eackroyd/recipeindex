from django.urls import path
from recipes.views import show_recipe, recipe_list, create_recipe, edit_recipe, my_recipe_list, view_shopping_list, breakfast_recipe_list, lunch_recipe_list, dinner_recipe_list, baked_goods_recipe_list

#in order for urls.py to call the function, import from views. need this file to fire
#uses endpoints from the browser url to fire a specific view
#empty str = whenever hit endpoint fire this function

urlpatterns = [
    path('<int:id>/', show_recipe, name='show_recipe'),
    path("", recipe_list, name='main_page'),
    path("create/", create_recipe, name="create_recipe"),
    path("<int:id>/edit.html", edit_recipe, name="edit_recipe"),
    path('mine/', my_recipe_list, name="my_recipe_list"),
    path('shopping_list/', view_shopping_list, name="shopping_list"),
    path('breakfast/', breakfast_recipe_list, name="breakfast"),
    path('lunch/', lunch_recipe_list, name="lunch"),
    path('dinner/', dinner_recipe_list, name="dinner"),
    path('baked_goods/', baked_goods_recipe_list, name="baked_goods"),
]
