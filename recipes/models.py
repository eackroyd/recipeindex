from django.db import models
from django.conf import settings

# Create your models here.

class TypeOfMeal(models.Model):
    meal_type = models.CharField(max_length = 100)
    def __str__(self):
        return self.meal_type

class Recipe(models.Model):
    title=models.CharField(max_length = 200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name = "recipes",
        on_delete = models.CASCADE,
        null=True
    )
    type = models.ForeignKey(
        "TypeOfMeal",
        related_name = "recipes",
        on_delete = models.CASCADE,
        null=True,
    )
    def __str__(self):
        return self.title

class RecipeStep(models.Model):
    instruction = models.TextField()
    order = models.PositiveSmallIntegerField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",
        on_delete=models.CASCADE,
        null=True,
    )
    class Meta:
        ordering = ["order"]

class Ingredients(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        "Recipe",
        related_name = "ingredients",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta:
        ordering = ["food_item"]

class ShoppingList(models.Model):
    date = models.DateField(auto_now_add=True)
    def __str__(self):
        return "Week of " + self.date.strftime('%B %d')

class ShoppingListItem(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    shopping_list = models.ForeignKey(
        "ShoppingList",
        related_name = "shopping_list_item",
        on_delete=models.CASCADE,
        null=True
    )
